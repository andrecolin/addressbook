
## Simple Getting Started Ionic App
## with Rails 5 API Backend


### Create Backend with mysql
Require Ruby,Rails,Mysql
```
rails new ibackend --api -d mysql
edit config/database.yml #update mysql password
rails db:create
```
Create Rails Model
```
rails generate scaffold Address firstname:string lastname:string street:string city:string state:string zipcode:string phone:string
rails db:migrate
```
Start Rails Serve
```
cd ibackend
rails server
```
