import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class BackEndService {
  private url: string = 'http://localhost:1337/localhost:3000';
  constructor(private http: Http) {
  }
  postapi(urlpath: string, payload: any): any {
    // set the headers
    let headers: any = new Headers();
    headers.append('Content-Type', 'application/json');
    
    let fullPath: string = this.url.concat(urlpath);
    console.log(fullPath);

    let options = new RequestOptions({headers: headers});

    return this.http.post(fullPath, JSON.stringify(payload),
      options);
  }
}