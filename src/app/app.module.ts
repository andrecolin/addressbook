import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { NewAddress } from '../pages/newAddress/newAddress';
import { DisplayAddress } from '../pages/displayAddress/displayAddress';
import { BackEndService } from '../services/backend';

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    NewAddress,
    DisplayAddress
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    NewAddress,
    DisplayAddress
  ],
  providers: [BackEndService, {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
