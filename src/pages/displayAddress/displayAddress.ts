import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';


@Component({
  selector: 'page-displayAddress',
  templateUrl: 'displayAddress.html'
})
export class DisplayAddress {

        public address: any;

  constructor(public navCtrl: NavController, public navParams:NavParams) {
               this.address = navParams.get('address');
    
  }
}