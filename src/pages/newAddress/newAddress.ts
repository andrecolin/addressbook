import { Component } from '@angular/core';
import { BackEndService } from '../../services/backend';
import { NavController, LoadingController } from 'ionic-angular';
import { DisplayAddress} from '../displayAddress/displayAddress';

@Component({
  selector: 'page-newAddress',
  templateUrl: 'newAddress.html'
})
export class NewAddress {

        public firstname: string;
        public lastname: string;
        public address: any;

  constructor(public navCtrl: NavController, public loadingController: LoadingController,
                public backend: BackEndService) {

  }

addAddress() {

   console.log('IN ADDRESS');

   // give the user a spinning icon when going to backend
   //
   let loading = this.loadingController.create({
     content: 'Adding New Address...'
   });
   loading.present();

   // create javascript object to send to backend
   let payload: any = {firstname: this.firstname, lastname: this.lastname};

   this.backend.postapi('/addresses', payload)
     .subscribe(
       data => {

         // dumping return params
         console.log("DUMPING RETURN DATA FROM BACKEND");
         console.log(JSON.stringify(data.text()));

         this.address = data.json();
         
       },
       err => {
         loading.dismiss();
         console.error(err)
       },
       () => {
          // after successful ( no error) load this section
          // open the DisplayAddress page and send the
          // address object which is the returned data
         loading.dismiss();
             this.navCtrl.push(DisplayAddress, {
            address: this.address
        });
       });
 }

}
